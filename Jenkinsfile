pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                checkout scm: [
                    $class: 'GitSCM',
                    branches: [[name: '*/main']],
                    userRemoteConfigs: [[
                        url: 'git@gitlab.com:yourhostel.ua/hw-freestyle.git',
                        credentialsId: 'gitlab-ssh-key'
                    ]]
                ]
            }
        }

        stage('Build') {
            steps {
                script {
                    if (!fileExists('pipeline')) {
                        sh 'mkdir -p pipeline'
                    }
                def dateTime = sh(script: "date '+%Y-%m-%d_%H-%M-%S'", returnStdout: true).trim()
                writeFile file: "pipeline/${BUILD_NUMBER}-${dateTime}.txt", text: "Build ${BUILD_NUMBER} - ${dateTime}"
                sh 'ls -la pipeline'
                }
            }
        }

        stage('Publish') {
            steps {
               withCredentials([sshUserPrivateKey(credentialsId: 'gitlab-ssh-key', keyFileVariable: 'SSH_KEY')]) {
                  script {
                    sh 'git config user.email "jenkins@test.domain"'
                    sh 'git config user.name "Jenkins CI"'
                    sh 'git checkout main'
                    sh "GIT_SSH_COMMAND='ssh -i ${SSH_KEY}' git pull origin main"
                    sh "git add pipeline/*"
                    sh "git commit -m 'Add build ${env.BUILD_NUMBER}'"
                    sh "git tag v0.2.${env.BUILD_NUMBER}"
                    sh "GIT_SSH_COMMAND='ssh -i ${SSH_KEY}' git push origin main --tags"
                  }
               }
            }
        }
    }

    post {
        always {
            archiveArtifacts artifacts: 'hw-freestyle/pipeline/*.txt'
        }
    }
}
