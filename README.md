# Hw Freestyle 

## Basic
- Launch Jenkins
- Generate HTTPS creds to access git and import it to Jenkins secrets
- Create Freestyle job which will use the imported secret and clone any remote repository
- Go to the repository folder where:  
  - create folder `freestyle` if it is missing
  - create file with current data and time named `freestyle/${BUILD_NUMBER}.txt`
  - add new file/folder to git
  - commit change
  - add a tag constructed as `v0.1.${BUILD_NUMBER}`
  - push changes and tag to origin
- Add the created file as artifact of the job
- Run the job several times and confirm that project has multiple files and tags after that

```bash
# Очищення робочої області
rm -rf *

# Клонування репозиторію
git clone https://gitlab.com/yourhostel.ua/hw-freestyle.git

cd ./hw-freestyle

# Створення папки freestyle, якщо вона відсутня
mkdir -p freestyle

# Створення файлу з поточною датою та часом
DATE_TIME=$(date +"%Y-%m-%d_%H-%M-%S")
echo "Build ${BUILD_NUMBER} - ${DATE_TIME}" > freestyle/${BUILD_NUMBER}-${DATE_TIME}.txt

# Налаштування імені користувача та email для Git у поточному репозиторії
git config user.email "jenkins@test.domain"
git config user.name "Jenkins CI"

# Додавання нового файлу до Git
git add .

# Коміт змін
git commit -m "Added build ${BUILD_NUMBER}"

# Додавання тега
# Перевірка існування тега
if git rev-parse -q --verify "v0.1.${BUILD_NUMBER}" >/dev/null; then
  # Заміна існуючого тега
  git tag -a -f "v0.1.${BUILD_NUMBER}" -m "Build ${BUILD_NUMBER}"
else
  # Створення нового тега
  git tag -a "v0.1.${BUILD_NUMBER}" -m "Build ${BUILD_NUMBER}"
fi

# Перевірка наявності локальної ветки main і переключення на неї
git checkout main || git checkout -b main

# Push змін та тега в репозиторій
git push https://${GIT_USERNAME}:${GIT_PASSWORD}@gitlab.com/yourhostel.ua/hw-freestyle.git main
```
#### Task
![2024-03-15_220848.jpg](img%2F2024-03-15_220848.jpg)
![2024-03-15_221402.jpg](img%2F2024-03-15_221402.jpg)
![2024-03-15_221440.jpg](img%2F2024-03-15_221440.jpg)
![2024-03-15_221514.jpg](img%2F2024-03-15_221514.jpg)
#### Archive the artifacts
![2024-03-15_221914.jpg](img%2F2024-03-15_221914.jpg)
![2024-03-15_221934.jpg](img%2F2024-03-15_221934.jpg)

# Hw Pipeline

## Basic
- Launch Jenkins
- Generate SSH key and import it to Jenkins secrets
- Configure Pipeline job
- Configure downloading of pipeline code from repo using SSH authentication by using Jenkins secret
- The code should have the following stages:  
  - Checkout: get some project code from remote repo, use SSH here as well
  - Build: create folder and file with current data and time named `pipeline/${BUILD_NUMBER}.txt`
  - Publish: add the file to git, commit change, add tag `v0.2.${BUILD_NUMBER}`, push to origin
  - Post Build: add generated file as job artifact
- Run the job several times and confirm that project has multiple files and tags after that

![2024-03-16_025938.jpg](img%2F2024-03-16_025938.jpg)